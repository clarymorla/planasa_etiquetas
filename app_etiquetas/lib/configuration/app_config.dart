
import 'package:flutter/material.dart';

enum Flavor {
  DEV,
  PRO
}

class FlavorValues {
  final String baseURL;
  final String serviceUser;
  final String servicePass;

  FlavorValues({@required this.baseURL, this.serviceUser, this.servicePass});
}

class FlavorConfig {
  final Flavor flavor;
  final String name;
  final FlavorValues values;
  static FlavorConfig _instance;

  factory FlavorConfig({@required Flavor flavor, @required FlavorValues values, String name}) {
    _instance ??= FlavorConfig._internal(flavor, name, values);
    return _instance;
  }

  FlavorConfig._internal(this.flavor, this.name, this.values);
  static FlavorConfig get instance { return _instance;}
  static bool isProd() => _instance.flavor == Flavor.PRO;
  static bool isDev() => _instance.flavor == Flavor.DEV;
}