class Preferences {
  Preferences._();

  static const pref_themeDark = "themeDark";
  static const pref_userToken = "userToken";
  static const pref_user = "user";
  static const pref_password = "password";
  static const pref_remember = "rememberMe";
  static const pref_language = "language";

}