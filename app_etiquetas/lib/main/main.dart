import 'package:app_etiquetas/configuration/app_config.dart';
import 'package:app_etiquetas/configuration/preferences.dart';
import 'package:app_etiquetas/view/login_screen.dart';
import 'package:catcher/catcher.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized(); //fix to Unhandled Exception: ServicesBinding.defaultBinaryMessenger was accessed before the binding was initialized

  CatcherOptions debugOptions = CatcherOptions(DialogReportMode(), [ConsoleHandler()], localizationOptions: [LocalizationOptions.buildDefaultSpanishOptions()]);
  CatcherOptions releaseOptions =  CatcherOptions (SilentReportMode (),
      [EmailAutoHandler ( "smtp.gmail.com", 587, "notificaciones@onegolive.com", "Bugs Planasa Impresiones PD", "Aya_23652365",
          ["apps@onegolive.com","cristian.blanco@onegolive.com"]) ]);

  SharedPreferences prefs = await SharedPreferences.getInstance();

  var value = false;
  if (prefs.containsKey(Preferences.pref_themeDark))
    value = prefs.getBool(Preferences.pref_themeDark);

  FlavorConfig(
      flavor: Flavor.PRO,
      name: 'Impresiones',
      values: FlavorValues(
          baseURL: 'https://apppartes.planasa.com:8093/PD920/',
          serviceUser: 'JDE',
          servicePass: 'Pl@2365'
      )
  );

  Catcher(
      rootWidget: MyApp(),
      debugConfig: debugOptions,
      releaseConfig: releaseOptions);
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: FlavorConfig.instance.name,
      navigatorKey: Catcher.navigatorKey,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      routes: <String, WidgetBuilder>{
        "/": (BuildContext context) => LoginScreen(),
      },
    );
  }
}

