
import 'package:app_etiquetas/configuration/app_config.dart';
import 'package:app_etiquetas/configuration/preferences.dart';
import 'package:app_etiquetas/main/main.dart';
import 'package:catcher/catcher.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized(); //fix to Unhandled Exception: ServicesBinding.defaultBinaryMessenger was accessed before the binding was initialized

  CatcherOptions debugOptions = CatcherOptions(DialogReportMode(), [ConsoleHandler()], localizationOptions: [LocalizationOptions.buildDefaultSpanishOptions()]);
  CatcherOptions releaseOptions =  CatcherOptions (SilentReportMode (),
      [EmailAutoHandler ( "smtp.gmail.com", 587, "notificaciones@onegolive.com", "Bugs Planasa Impresiones DV", "Aya_23652365",
          ["apps@onegolive.com","cristian.blanco@onegolive.com"]) ]);

  SharedPreferences prefs = await SharedPreferences.getInstance();

  var value = false;
  if (prefs.containsKey(Preferences.pref_themeDark))
    value = prefs.getBool(Preferences.pref_themeDark);

  FlavorConfig(
      flavor: Flavor.DEV,
      name: 'DV Impresiones',
      values: FlavorValues(
          baseURL: 'https://jdedev01.planasa.lan:8091/DV920/',
          serviceUser: 'JDE',
          servicePass: 'Pl@2365'
      )
  );

  Catcher(
      rootWidget: MyApp(),
      debugConfig: debugOptions,
      releaseConfig: releaseOptions);
}